pragma solidity >=0.4.22 <0.9.0;

contract RepoInfo {
    string private  jsonInfo="{}";

    function setInfo(string  memory info)public returns(string memory) {
        jsonInfo=info;
    }

    function getInfo() public view returns(string memory){
        return jsonInfo;
    }
}


