package io.sdv.ekip.SupplyBlockChain;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import io.sdv.ekip.SupplyBlockChain.Model.Contract.RepoInfo;
import io.sdv.ekip.SupplyBlockChain.Model.ContractInfoModel;
import io.sdv.ekip.SupplyBlockChain.Model.ContractType;
import io.sdv.ekip.SupplyBlockChain.Service.ContractsManagerService;
import org.junit.jupiter.api.Test;
import org.web3j.EVMTest;
import org.web3j.crypto.Credentials;
import org.web3j.protocol.Web3j;
import org.web3j.tx.TransactionManager;
import org.web3j.tx.gas.ContractGasProvider;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertEquals;

////////////////////////////////


//To run the test you need to run ganache in docker with mnemonic param like this:
//docker run --detach --publish 8545:8545 trufflesuite/ganache-cli:latest --mnemonic blockchaintest


////////////////////////////////

@EVMTest
public class ContractManagerTest {

    @Test
    public void contractTransactionTest(Web3j web3j, TransactionManager transactionManager, ContractGasProvider gasProvider) throws Exception {

        RepoInfo contract = RepoInfo.deploy(web3j, transactionManager, gasProvider).send();
        contract.setInfo("testMsg").send();
        String msg = contract.getInfo().send();
        assertEquals("testMsg", msg);
    }

    @Test
    public void contractManagerTest() throws Exception {
        ContractsManagerService contractsManagerService = new ContractsManagerService();
        Credentials credentials = Credentials.create("0x0e36c04dfd0895ab8f2e094d7e2c0094b7e8b81bc711a6f84ee6d7c309830d8a");

        ObjectMapper mapper = new ObjectMapper();
        ObjectNode jsonData = mapper.createObjectNode();
        jsonData.put("key", "message test");
        ContractInfoModel contractInfo = new ContractInfoModel(jsonData, ContractType.COMMIT);

        RepoInfo contract = contractsManagerService.deployContractWithData(credentials, contractInfo);
        String msg = contract.getInfo().send();

        JsonNode rawData = mapper.readTree(msg);

        ContractInfoModel result =  mapper.treeToValue(rawData, ContractInfoModel.class);

        assertEquals("message test", result.getData().get("key").asText());
        assertEquals(ContractType.COMMIT, result.getContractType());
    }

    @Test
    public void latestBlockTest() throws Exception {
        ContractsManagerService contractsManagerService = new ContractsManagerService();
        Credentials credentials = Credentials.create("0x0e36c04dfd0895ab8f2e094d7e2c0094b7e8b81bc711a6f84ee6d7c309830d8a");

        ObjectMapper mapper = new ObjectMapper();
        ObjectNode jsonData = mapper.createObjectNode();
        jsonData.put("key", "latest test");
        ContractInfoModel contractInfo = new ContractInfoModel(jsonData, ContractType.PIPELINE);

        contractsManagerService.deployContractWithData(credentials, contractInfo);

        ContractInfoModel result =  contractsManagerService.getLatestContract(credentials);

        assertEquals("latest test", result.getData().get("key").asText());
        assertEquals(ContractType.PIPELINE, result.getContractType());
    }

    @Test
    public void contractListTest() throws Exception {
        ContractsManagerService contractsManagerService = new ContractsManagerService();
        Credentials credentials = Credentials.create("0x0e36c04dfd0895ab8f2e094d7e2c0094b7e8b81bc711a6f84ee6d7c309830d8a");

        ObjectMapper mapper = new ObjectMapper();
        ObjectNode jsonData = mapper.createObjectNode();
        jsonData.put("key", "contract list");
        ContractInfoModel contractInfo = new ContractInfoModel(jsonData, ContractType.COMMIT);

        contractsManagerService.deployContractWithData(credentials, contractInfo);

        ArrayList<ContractInfoModel> contractList = contractsManagerService.getAllContract(credentials);

        ContractInfoModel latestContract = contractList.get(contractList.size() - 1);

        assertEquals("contract list", latestContract.getData().get("key").asText());
        assertEquals(ContractType.COMMIT, latestContract.getContractType());
    }

    @Test
    public void latestContractInfoTest() throws Exception {
        ContractsManagerService contractsManagerService = new ContractsManagerService();
        Credentials credentials = Credentials.create("0x0e36c04dfd0895ab8f2e094d7e2c0094b7e8b81bc711a6f84ee6d7c309830d8a");

        ObjectMapper mapper = new ObjectMapper();
        ObjectNode jsonData = mapper.createObjectNode();
        jsonData.put("key", "direct info");
        ContractInfoModel contractInfo = new ContractInfoModel(jsonData, ContractType.COMMIT);

        contractsManagerService.deployContractWithData(credentials, contractInfo);

        ContractInfoModel contractInfoRes = contractsManagerService.getLatestContract(credentials);

        assertEquals("direct info", contractInfoRes.getData().get("key").asText());
        assertEquals(ContractType.COMMIT, contractInfoRes.getContractType());
    }

    @Test
    public void allContractInfoTest() throws Exception {
        ContractsManagerService contractsManagerService = new ContractsManagerService();
        Credentials credentials = Credentials.create("0x0e36c04dfd0895ab8f2e094d7e2c0094b7e8b81bc711a6f84ee6d7c309830d8a");

        ObjectMapper mapper = new ObjectMapper();
        ObjectNode jsonData = mapper.createObjectNode();
        jsonData.put("key", "last info");
        ContractInfoModel contractInfo = new ContractInfoModel(jsonData, ContractType.COMMIT);

        contractsManagerService.deployContractWithData(credentials, contractInfo);

        ArrayList<ContractInfoModel> contractInfoList = contractsManagerService.getAllContract(credentials);

        ContractInfoModel lastContract = contractInfoList.get(contractInfoList.size() - 1);

        assertEquals("last info", lastContract.getData().get("key").asText());
        assertEquals(ContractType.COMMIT, lastContract.getContractType());
    }

    @Test
    public void testContractAddr() throws Exception {
        ContractsManagerService contractsManagerService = new ContractsManagerService();
        Credentials credentials = Credentials.create("0x0e36c04dfd0895ab8f2e094d7e2c0094b7e8b81bc711a6f84ee6d7c309830d8a");

        String addr = contractsManagerService.calculateContractAddress(credentials.getAddress(), 2);

        ContractInfoModel contract = contractsManagerService.getContractByAddress(addr, credentials);

    }

}
