package io.sdv.ekip.SupplyBlockChain.Jobs;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.sdv.ekip.SupplyBlockChain.Model.*;
import io.sdv.ekip.SupplyBlockChain.Service.ContractsManagerService;
import io.sdv.ekip.SupplyBlockChain.Service.GitlabAPIService;
import io.sdv.ekip.SupplyBlockChain.Service.GitlabRedisService;
import io.sdv.ekip.SupplyBlockChain.Utils.GitlabUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.web3j.crypto.Credentials;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;

@Service
public class GitlabJob {

    private final Logger logger = LoggerFactory.getLogger(GitlabJob.class);

    private final ObjectMapper mapper = new ObjectMapper();
    private final GitlabAPIService gitlabAPIService = new GitlabAPIService();
    private final GitlabRedisService gitlabRedisService = new GitlabRedisService();
    private final GitlabUtils gitlabUtils = new GitlabUtils();
    private final ContractsManagerService contractsManager = new ContractsManagerService();
    private final SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss");

    Credentials credentials = Credentials.create("0x59edc805b16cc8c2d111a63c90337ab09868c010e0a96832ef422862c3a96553");

    @Scheduled(fixedRate = 10000)
    public void gitlabInfoToBlockchain() {

        logger.info("Starting Job");
        long startTime = System.currentTimeMillis();

        JsonNode repo = gitlabRedisService.getRepos();

        String repoId = repo.get(GitlabRedisService.Key.REPO_ID.getKey()).asText();
        String repoKey = repo.get(GitlabRedisService.Key.REPO_KEY.getKey()).asText();

        if(repoId.equals("null") || repoKey.equals("null"))
        {
            logger.info("Stopping Job because gitlab repo info is missing from redis");
            return;
        }

        try {

            this.addCommits(repoId, repoKey);
            this.addPipelines(repoId, repoKey);

        } catch (Exception exception) {
            logger.info("Error while executing job");
            exception.printStackTrace();
        }

        long endTime = System.currentTimeMillis();
        logger.info("Job finished in milliseconds: {}", endTime - startTime);

    }

    public void addCommits(String repoId, String repoKey) throws Exception {
        logger.info("Adding commits");

        ArrayList<JsonNode> commits = null;
        ArrayList<JsonNode> commitsToInsert = null;
        ArrayList<CommitInfo> commitList = new ArrayList<>();

        commits = gitlabAPIService.getCommits(repoId, repoKey);
        ContractInfoModel contractCommit = contractsManager.getLastInsertedType(credentials, ContractType.COMMIT);

        if(contractCommit != null) {
            String lastCommitId =  contractCommit.getData().get("id").textValue();
            commitsToInsert = gitlabUtils.getCommitsToInsert(commits, lastCommitId);
        } else {
            Collections.reverse(commits);
            commitsToInsert = commits;
        }

        for (JsonNode commitJson: commitsToInsert) {
            CommitInfo commit = new CommitInfo();
            commit.setId(commitJson.get("id").textValue());
            commit.setAuthor(commitJson.get("committer_email").textValue());
            commit.setCommitDate(commitJson.get("committed_date").textValue());
            commit.setMessage(commitJson.get("message").textValue());
            commitList.add(commit);
        }

        for (CommitInfo commit: commitList) {
            Date today = new Date();
            ContractInfoModel commitContract = new ContractInfoModel();
            commitContract.setData(mapper.valueToTree(commit));
            commitContract.setContractType(ContractType.COMMIT);
            commitContract.setInsertedDate(formatter.format(today));
            contractsManager.deployContractWithData(credentials, commitContract);
        }

        logger.info("Adding commits finished");
    }

    public void addPipelines(String repoId, String repoKey) throws Exception {
        logger.info("Adding pipelines");

        ArrayList<JsonNode> pipelines = null;
        ArrayList<JsonNode> pipelinesToInsert = null;
        ArrayList<PipelineInfo> pipelineList = new ArrayList<>();

        pipelines = gitlabAPIService.getSaFPipelines(repoId, repoKey);
        ContractInfoModel contractPipeline = contractsManager.getLastInsertedType(credentials, ContractType.PIPELINE);

        if(contractPipeline != null) {
            String lastPipelineId =  contractPipeline.getData().get("id").textValue();
            pipelinesToInsert = gitlabUtils.getPipelinesToInsert(pipelines, lastPipelineId);
        } else {
            Collections.reverse(pipelines);
            pipelinesToInsert = pipelines;
        }

        for (JsonNode pipelineJson: pipelinesToInsert) {
            ArrayList<StageInfo> stages = new ArrayList<>();
            PipelineInfo pipeline = new PipelineInfo();

            JsonNode jobsJson = gitlabAPIService.getJobs(pipelineJson.get("id").intValue(), repoId, repoKey);
            for (JsonNode stage: jobsJson) {
                StageInfo stageInfo = new StageInfo(stage.get("name").textValue(), stage.get("status").textValue());
                stages.add(stageInfo);
            }

            pipeline.setId(pipelineJson.get("sha").textValue());
            pipeline.setStatus(pipelineJson.get("status").textValue());
            pipeline.setAuthor(pipelineJson.get("user").get("name").textValue());
            pipeline.setEndDate(pipelineJson.get("finished_at").textValue());
            pipeline.setJobs(stages);
            pipelineList.add(pipeline);
        }

        for (PipelineInfo pipeline: pipelineList) {
            Date today = new Date();
            ContractInfoModel pipelineContract = new ContractInfoModel();
            pipelineContract.setData(mapper.valueToTree(pipeline));
            pipelineContract.setContractType(ContractType.PIPELINE);
            pipelineContract.setInsertedDate(formatter.format(today));
            contractsManager.deployContractWithData(credentials, pipelineContract);
        }

        logger.info("Adding pipelines finished");
    }

}
