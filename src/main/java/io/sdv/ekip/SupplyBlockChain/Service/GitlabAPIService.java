package io.sdv.ekip.SupplyBlockChain.Service;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import okhttp3.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.ArrayList;

@Service
public class GitlabAPIService {

    private final Logger logger = LoggerFactory.getLogger(GitlabAPIService.class);

    private final OkHttpClient client = new OkHttpClient();
    private final ObjectMapper mapper = new ObjectMapper();

    public static final MediaType JSON
            = MediaType.get("application/json; charset=utf-8");

    public JsonNode getRepositoryInfo(String repoId, String apiKey) throws IOException {
        HttpUrl url = new HttpUrl.Builder()
                .scheme("https")
                .host("gitlab.com")
                .addPathSegments("/api/v4/projects/"+ repoId)
                .build();

        Request request = new Request.Builder()
                .url(url)
                .addHeader("PRIVATE-TOKEN", apiKey)
                .build();

        Call call = client.newCall(request);

        Response response = call.execute();

        if (response.isSuccessful()) {
            ResponseBody res = response.body();
            return mapper.readTree(res.string());
        }

        throw new IOException();
    }

    public ArrayList<JsonNode> getCommits(String repoId, String apiKey) throws IOException {

        ArrayList<JsonNode> commits = new ArrayList<>();

        for(int i = 1; ; i++) {

            HttpUrl url = new HttpUrl.Builder()
                    .scheme("https")
                    .host("gitlab.com")
                    .addPathSegments("/api/v4/projects/"+ repoId +"/repository/commits")
                    .addQueryParameter("page", String.valueOf(i))
                    .build();

            Request request = new Request.Builder()
                    .url(url)
                    .addHeader("PRIVATE-TOKEN", apiKey)
                    .build();

            Call call = client.newCall(request);

            Response response = call.execute();
            String nextPage = response.header("x-next-page");

            if (response.isSuccessful()) {
                ResponseBody res = response.body();

                JsonNode jsonCommits = mapper.readTree(res.string());

                for(JsonNode commit: jsonCommits) {
                    commits.add(commit);
                }

            }
            if(nextPage == null) {
                throw new IOException("Problem in repo setup");
            }
            if(nextPage.equals("")) {
                break;
            }

        }

        return commits;
    }

    public JsonNode getLastCommit(String repoId, String apiKey) throws Exception {
       ArrayList<JsonNode> commits = null;

        try {
            commits = this.getCommits(repoId, apiKey);
        } catch (IOException exception) {
            throw new Exception("Error while getting commits for repo: " + repoId);
        }

        if (!commits.isEmpty()) {
            return commits.get(0);
        }

        return null;
    }

    public ArrayList<JsonNode> getPipelines(String repoId, String apiKey) throws IOException {

        ArrayList<JsonNode> pipelines = new ArrayList<>();

        for(int i = 1; ; i++) {
            HttpUrl url = new HttpUrl.Builder()
                    .scheme("https")
                    .host("gitlab.com")
                    .addPathSegments("api/v4/projects/"+ repoId +"/pipelines")
                    .addQueryParameter("per_page", String.valueOf(100))
                    .addQueryParameter("page", String.valueOf(i))
                    .build();
            ;
            Request request = new Request.Builder()
                    .url(url)
                    .addHeader("PRIVATE-TOKEN", apiKey)
                    .build();

            Call call = client.newCall(request);
            Response response = call.execute();
            String nextPage = response.header("x-next-page");

            if (response.isSuccessful()) {
                ResponseBody res = response.body();

                JsonNode jsonPipelines = mapper.readTree(res.string());

                for (JsonNode pipeline : jsonPipelines) {
                    pipelines.add(pipeline);
                }

            }
            if(nextPage == null) {
                throw new IOException("Problem in repo setup");
            }
            if (nextPage.equals("")) {
                break;
            }
        }

        return pipelines;
    }

    /**
     * @return return only succeed and failed pipelines
     * @throws IOException
     */
    public ArrayList<JsonNode> getSaFPipelines(String repoId, String apiKey) throws IOException {

        ArrayList<JsonNode> SaFPipelines = new ArrayList<>();
        ArrayList<JsonNode> allPipelines= this.getPipelines(repoId, apiKey);

        String status = null;

        for (JsonNode node : allPipelines) {
            status = node.get("status").asText();
            if (status.equals("failed") || status.equals("success")) {
                JsonNode fullNode = this.getPipelineById(node.get("id").asText(), repoId, apiKey);
                SaFPipelines.add(fullNode);
            }
        }

        return SaFPipelines;
    }

    /**
     * @param id id of the pipeline
     * @return return full info of a specific pipeline
     * @throws IOException
     */
    public JsonNode getPipelineById(String id, String repoId, String apiKey) throws IOException {

        HttpUrl url = new HttpUrl.Builder()
                .scheme("https")
                .host("gitlab.com")
                .addPathSegments("/api/v4/projects/"+ repoId +"/pipelines/" + id)
                .build();

        Request request = new Request.Builder()
                .url(url)
                .addHeader("PRIVATE-TOKEN", apiKey)
                .build();

        Call call = client.newCall(request);
        Response response = call.execute();
        if (response.isSuccessful()) {
            ResponseBody res = response.body();
            return mapper.readTree(res.string());
        }

        return null;
    }

    public JsonNode getJobs(int pipelineId, String repoId, String apiKey) throws IOException {

        HttpUrl url = new HttpUrl.Builder()
                .scheme("https")
                .host("gitlab.com")
                .addPathSegments("/api/v4/projects/"+ repoId +"/pipelines/" + pipelineId + "/jobs")
                .build();

        Request request = new Request.Builder()
                .url(url)
                .addHeader("PRIVATE-TOKEN", apiKey)
                .build();

        Call call = client.newCall(request);
        Response response = call.execute();
        if (response.isSuccessful()) {
            ResponseBody res = response.body();
            return mapper.readTree(res.string());
        }

        return null;

    }

}
