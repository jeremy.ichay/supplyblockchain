package io.sdv.ekip.SupplyBlockChain.Service;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.sdv.ekip.SupplyBlockChain.Model.Contract.RepoInfo;
import io.sdv.ekip.SupplyBlockChain.Model.ContractInfoModel;
import io.sdv.ekip.SupplyBlockChain.Model.ContractType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.web3j.crypto.Credentials;
import org.web3j.crypto.Hash;
import org.web3j.protocol.Web3j;
import org.web3j.protocol.core.DefaultBlockParameter;
import org.web3j.protocol.core.methods.response.EthBlock;
import org.web3j.protocol.core.methods.response.EthBlockNumber;
import org.web3j.protocol.core.methods.response.Web3ClientVersion;
import org.web3j.protocol.http.HttpService;
import org.web3j.rlp.RlpEncoder;
import org.web3j.rlp.RlpList;
import org.web3j.rlp.RlpString;
import org.web3j.tx.gas.ContractGasProvider;
import org.web3j.tx.gas.StaticGasProvider;
import org.web3j.utils.Numeric;

import java.io.IOException;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;

public class ContractsManagerService {

    private final Logger logger = LoggerFactory.getLogger(GitlabAPIService.class);

    private final Web3j web3j = Web3j.build(new HttpService());
    private final BigInteger GAS_LIMIT = BigInteger.valueOf(6721975);
    private final BigInteger GAS_PRICE = BigInteger.valueOf(20000000000L);
    private final ContractGasProvider contractGasProvider = new StaticGasProvider(GAS_PRICE, GAS_LIMIT);
    private final ObjectMapper mapper = new ObjectMapper();


    public String getWeb3jVersion() throws IOException {
        Web3ClientVersion web3ClientVersion = web3j.web3ClientVersion().send();
        return web3ClientVersion.getWeb3ClientVersion();
    }

    /**
     * @return return the number of contract deployed in the chain without transaction counted
     * @throws IOException
     */
    public int getNumberOfDeployedContracts() throws IOException {
        return web3j.ethBlockNumber().send().getBlockNumber().intValue() / 2;
    }

    /**
     * @param senderAddress Address of the contract sender
     * @param nonce number of the contract that you want to get
     * @return return the calculated address
     */
    public String calculateContractAddress(String senderAddress, long nonce){

        long calculatedNonce = (nonce * 2) - 2;

        byte[] addressAsBytes = Numeric.hexStringToByteArray(senderAddress);

        byte[] calculatedAddressAsBytes =
                Hash.sha3(RlpEncoder.encode(
                        new RlpList(
                                RlpString.create(addressAsBytes),
                                RlpString.create((calculatedNonce)))));

        calculatedAddressAsBytes = Arrays.copyOfRange(calculatedAddressAsBytes,
                12, calculatedAddressAsBytes.length);
        return Numeric.toHexString(calculatedAddressAsBytes);
    }

    /**
     * @param credentials credentials of the sender
     * @param data Data that you want to add as payload in the contract
     * @return return the created contract
     * @throws Exception
     */
    public RepoInfo deployContractWithData(Credentials credentials, ContractInfoModel data) throws Exception {

        RepoInfo contract = null;
        logger.info("Deploying contract");
        try {
            String contractAddr = RepoInfo.deploy(this.web3j, credentials, this.contractGasProvider).send().getContractAddress();
            contract = RepoInfo.load(contractAddr, this.web3j, credentials, this.contractGasProvider);
            JsonNode jsonData = mapper.valueToTree(data);
            contract.setInfo(jsonData.toString()).send();
            logger.info("Contract deployed at: " + contractAddr);
        } catch (Exception exception) {
            throw new Exception("Error while processing contract: " + exception.getMessage());
        }

        return contract;
    }

    /**
     * @param addr address of the deployed contract
     * @param credentials credentials of the user who send the contract to interact with it
     * @return return the data in the contract as a model
     * @throws Exception
     */
    public ContractInfoModel getContractByAddress(String addr, Credentials credentials) throws Exception {

        RepoInfo contract = RepoInfo.load(addr, this.web3j, credentials, this.contractGasProvider);

        String rawInfo = contract.getInfo().send();
        JsonNode rawData = mapper.readTree(rawInfo);
        ContractInfoModel contractModel = mapper.treeToValue(rawData, ContractInfoModel.class);
        contractModel.setContractAddresse(addr);
        return contractModel;
    }

    public ArrayList<ContractInfoModel> getAllContract(Credentials credentials) throws Exception {
        ArrayList<ContractInfoModel> contractInfoList = new ArrayList<>();
        logger.info("Getting all contract");

        long startTime = System.currentTimeMillis();

        int numberContract = this.getNumberOfDeployedContracts();

        for(int i = 1; i <= numberContract; i++) {
            String addr = this.calculateContractAddress(credentials.getAddress(), i);
            contractInfoList.add(getContractByAddress(addr, credentials));
        }

        long endTime = System.currentTimeMillis();
        logger.info("Execution time in milliseconds: {}", endTime - startTime);

        return contractInfoList;
    }

    public ContractInfoModel getLatestContract(Credentials credentials) throws Exception {

        long startTime = System.currentTimeMillis();
        logger.info("Getting last contract");
        try {
            int numberContract = this.getNumberOfDeployedContracts();

            if(numberContract == 0) return null; //return null in case there is no contract

            String addr = this.calculateContractAddress(credentials.getAddress(), numberContract);

            ContractInfoModel contract = this.getContractByAddress(addr, credentials);

            long endTime = System.currentTimeMillis();
            logger.info("Execution time in milliseconds: {}", endTime - startTime);

            return contract;

        } catch (Exception exception) {
            throw new Exception("Error while getting last contract");
        }

    }

    /**
     * @param credentials credentials to interact with the contract
     * @param type type of the last contract you want to get
     * @return return the latest contract inserted according the type in parameter
     * @throws Exception
     */
    public ContractInfoModel getLastInsertedType(Credentials credentials, ContractType type) throws Exception {
        ArrayList<ContractInfoModel> contractList =  this.getAllContract(credentials);
        Collections.reverse(contractList);
        for(ContractInfoModel contract: contractList) {
            if(contract.getContractType() == type) {
                return contract;
            }
        }
        return null;
    }

    //////////////////////////////////////////////////////////////////
    /*                       Old code                               */
    //////////////////////////////////////////////////////////////////

    /**
     * @param nbBlock block number
     * @return return block of any type
     * @throws IOException
     */
    public EthBlock.Block getBlock(BigInteger nbBlock) throws IOException {

        return web3j.ethGetBlockByNumber(DefaultBlockParameter.valueOf(nbBlock), true).send().getBlock();
    }

    @Deprecated
    public EthBlock.Block getLatestBlock() throws IOException {

        try {

            EthBlockNumber blockNumber = web3j.ethBlockNumber().send();
            return this.getBlock(blockNumber.getBlockNumber());

        } catch (IOException exception) {
            throw new IOException("Can't get latest block: " + exception.getMessage());
        }
    }

    @Deprecated
    public ArrayList<RepoInfo> getAllContractInBlockchain(Credentials credentials) throws IOException {

        logger.info("Getting all contract");
        long startTime = System.currentTimeMillis();

        int maxBlock = 0;
        ArrayList<RepoInfo> contractsList = new ArrayList<>();

        maxBlock = getLatestBlock().getNumber().intValue() + 1;

        for (int i = 1; i < maxBlock; i++) {

            BigInteger numBlock = BigInteger.valueOf(i);
            this.getBlock(numBlock);
            String addrBlock = ((EthBlock.TransactionObject) this.getBlock(BigInteger.valueOf(i)).getTransactions().get(0)).getTo();

            if (addrBlock != null) {
                contractsList.add(RepoInfo.load(addrBlock, this.web3j, credentials, this.contractGasProvider));
            }
        }

        long endTime = System.currentTimeMillis();
        logger.info("Execution time in milliseconds: {}", endTime - startTime);

        return contractsList;
    }

    @Deprecated
    public RepoInfo getLatestContractInBlockchain(Credentials credentials) throws IOException {

        int maxBlock = getLatestBlock().getNumber().intValue();

        RepoInfo RepoInfo = null;

        int count = maxBlock;

        while (RepoInfo == null && count > 0) {

            BigInteger numBlock = BigInteger.valueOf(count);
            this.getBlock(numBlock);
            String addrBlock = ((EthBlock.TransactionObject) this.getLatestBlock().getTransactions().get(0)).getTo();

            if (addrBlock != null) {
                RepoInfo = RepoInfo.load(addrBlock, this.web3j, credentials, this.contractGasProvider);
            }

            count--;
        }

        return RepoInfo;
    }

    @Deprecated
    public ContractInfoModel getLatestContractOld(Credentials credentials) throws Exception {

        try {
            RepoInfo repoInfo = this.getLatestContractInBlockchain(credentials);
            String rawData = repoInfo.getInfo().send();
            JsonNode jsonData = mapper.readTree(rawData);
            return  mapper.treeToValue(jsonData, ContractInfoModel.class);
        } catch (Exception exception) {
            throw new Exception("Error while getting last contract");
        }

    }

    @Deprecated
    public ArrayList<ContractInfoModel> getAllContractOld(Credentials credentials) throws Exception {
        ArrayList<RepoInfo> repoInfoList = null;
        ArrayList<ContractInfoModel> contractInfoModels = new ArrayList<>();

        long startTime = System.currentTimeMillis();

        try {
            repoInfoList = this.getAllContractInBlockchain(credentials);

            for (RepoInfo repoInfo: repoInfoList) {
                String rawData = repoInfo.getInfo().send();
                JsonNode jsonData = mapper.readTree(rawData);
                contractInfoModels.add(mapper.treeToValue(jsonData, ContractInfoModel.class));
            }

        } catch (Exception exception) {
            throw new Exception("Error while getting all contract");
        }

        long endTime = System.currentTimeMillis();
        logger.info("Execution time in milliseconds: {}", endTime - startTime);

        return contractInfoModels;
    }

}