package io.sdv.ekip.SupplyBlockChain.Service;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import redis.clients.jedis.Jedis;

import java.io.IOException;

public class GitlabRedisService {

    public enum Key {

        REPO_ID("id"),
        REPO_KEY("apiKey");

        private String key;

        Key(String key) {
            this.key = key;
        }

        public String getKey() {
            return key;
        }
    }

    private Jedis jedis = new Jedis();
    private final GitlabAPIService gitlabAPIService = new GitlabAPIService();
    private ObjectMapper mapper = new ObjectMapper();

    public JsonNode getRepos() {

        ObjectNode res = mapper.createObjectNode();

        res.put("id", jedis.get("gitlab-repo-id"));
        res.put("apiKey", jedis.get("gitlab-repo-key"));

        return res;
    }

    public JsonNode addRepos(JsonNode json) throws IllegalArgumentException {

        if( json.has("id") && json.has("apiKey") ) {

            String repoId = json.get("id").textValue();
            String apiKey = json.get("apiKey").textValue();

            try {
                gitlabAPIService.getRepositoryInfo(repoId, apiKey);
            } catch (IOException exception) {
                throw new IllegalArgumentException("Repo Id or API Key is not correct");
            }

            jedis.set("gitlab-repo-id", repoId);
            jedis.set("gitlab-repo-key", apiKey);

            return json;
        } else {
            throw new IllegalArgumentException("Sent data doesn't contain required values");
        }
    }

}
