package io.sdv.ekip.SupplyBlockChain.Utils;

import com.fasterxml.jackson.databind.JsonNode;

import java.util.ArrayList;

public class GitlabUtils {

    /**
     * @param commits commits list from the project
     * @param lastCommitIdInserted last commit inserted in the blockchain
     * @return return a sub array of commits to insert in the blockchain
     */

    public ArrayList<JsonNode> getCommitsToInsert(ArrayList<JsonNode> commits, String lastCommitIdInserted) {
        ArrayList<JsonNode> commitsToinsert = new ArrayList<>();

        for (JsonNode commit : commits) {
            if(commit.get("id").textValue().equals(lastCommitIdInserted)) {
                break;
            } else {
                commitsToinsert.add(commit);
            }
        }

        return commitsToinsert;
    }

    /**
     * @param pipelines pipelines list from the project
     * @param lastPipelineIdInserted last pipeline inserted in the blockchain
     * @return return a sub array of pipelines to insert in the blockchain
     */
    public ArrayList<JsonNode> getPipelinesToInsert(ArrayList<JsonNode> pipelines, String lastPipelineIdInserted) {
        ArrayList<JsonNode> pipelinesToinsert = new ArrayList<>();

        for (JsonNode pipeline : pipelines) {

            if(pipeline.path("sha").textValue().equals(lastPipelineIdInserted)) {
                break;
            } else {
                pipelinesToinsert.add(pipeline);
            }
        }

        return pipelinesToinsert;
    }

}
