package io.sdv.ekip.SupplyBlockChain.Model;

public class StageInfo {
    private String jobName;
    private String jobStatus;

    public StageInfo() {
    }

    public StageInfo(String jobName, String jobStatus) {
        this.jobName = jobName;
        this.jobStatus = jobStatus;
    }

    public String getJobName() {
        return jobName;
    }

    public void setJobName(String jobName) {
        this.jobName = jobName;
    }

    public String getJobStatus() {
        return jobStatus;
    }

    public void setJobStatus(String jobStatus) {
        this.jobStatus = jobStatus;
    }
}
