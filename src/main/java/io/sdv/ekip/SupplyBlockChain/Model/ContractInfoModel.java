package io.sdv.ekip.SupplyBlockChain.Model;

import com.fasterxml.jackson.databind.JsonNode;

public class ContractInfoModel {

    private String contractAddresse;
    private JsonNode data;
    private ContractType contractType;
    private String insertedDate;

    /**
     * This constructor is needed for Jackson json conversion
     */
    public ContractInfoModel() {}

    public ContractInfoModel(JsonNode data, ContractType contractType) {
        this.data = data;
        this.contractType = contractType;
    }

    public ContractInfoModel(String contractAddresse, JsonNode data, ContractType contractType, String insertedDate) {
        this.contractAddresse = contractAddresse;
        this.data = data;
        this.contractType = contractType;
        this.insertedDate = insertedDate;
    }

    public JsonNode getData() {
        return data;
    }

    public void setData(JsonNode data) {
        this.data = data;
    }

    public ContractType getContractType() {
        return contractType;
    }

    public void setContractType(ContractType contractType) {
        this.contractType = contractType;
    }

    public String getContractAddresse() {
        return contractAddresse;
    }

    public void setContractAddresse(String contractAddresse) {
        this.contractAddresse = contractAddresse;
    }

    public void setInsertedDate(String insertedDate) { this.insertedDate = insertedDate; }

    public String getInsertedDate() { return this.insertedDate; }
}
