package io.sdv.ekip.SupplyBlockChain.Model;

public class CommitInfo {

    private String id;
    private String author;
    private String commitDate;
    private String message;

    public CommitInfo() {
    }

    public CommitInfo(String id, String author, String commitDate, String message) {
        this.id = id;
        this.author = author;
        this.commitDate = commitDate;
        this.message = message;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getCommitDate() {
        return commitDate;
    }

    public void setCommitDate(String commitDate) {
        this.commitDate = commitDate;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
