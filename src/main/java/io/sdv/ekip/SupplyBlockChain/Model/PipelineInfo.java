package io.sdv.ekip.SupplyBlockChain.Model;

import java.util.List;

public class PipelineInfo {

    private String id;
    private String author;
    private String endDate;
    private String status;
    private List<StageInfo> jobs;

    public PipelineInfo() {
    }

    public PipelineInfo(String id, String author, String endDate, String status, List<StageInfo> jobs) {
        this.id = id;
        this.author = author;
        this.endDate = endDate;
        this.status = status;
        this.jobs = jobs;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<StageInfo> getJobs() {
        return jobs;
    }

    public void setJobs(List<StageInfo> jobs) {
        this.jobs = jobs;
    }
}
