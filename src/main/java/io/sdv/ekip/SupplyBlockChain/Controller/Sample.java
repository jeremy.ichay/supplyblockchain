package io.sdv.ekip.SupplyBlockChain.Controller;

import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api")
public class Sample {

    @GetMapping("/sample")
    public String sampleGet(){
        return "ok";
    }

    @PostMapping("/sample")
    public String sampleGet(@RequestBody int value){
        return "ok";
    }

}
