package io.sdv.ekip.SupplyBlockChain.Controller.API;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.sdv.ekip.SupplyBlockChain.Model.Error.ApiErrorModel;
import io.sdv.ekip.SupplyBlockChain.Service.GitlabAPIService;
import io.sdv.ekip.SupplyBlockChain.Service.GitlabRedisService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;

@RestController
@RequestMapping("/api/gitlab")
public class GilabController {

    private final Logger logger = LoggerFactory.getLogger(GilabController.class);

    private final GitlabRedisService gitlabRedisService = new GitlabRedisService();
    private final GitlabAPIService gitlabAPIService = new GitlabAPIService();
    private final ObjectMapper mapper = new ObjectMapper();

    @GetMapping("/repos/current")
    public JsonNode getRepos() {
        return gitlabRedisService.getRepos();
    }

    @GetMapping("/repos/info")
    public ResponseEntity<JsonNode> getReposInfo() {
        JsonNode repo = gitlabRedisService.getRepos();
        String repoId = repo.get(GitlabRedisService.Key.REPO_ID.getKey()).asText();
        String repoKey = repo.get(GitlabRedisService.Key.REPO_KEY.getKey()).asText();
        try {
            JsonNode res = gitlabAPIService.getRepositoryInfo(repoId, repoKey);
            return ResponseEntity.ok(res);
        } catch (IOException exception) {
            ApiErrorModel error = new ApiErrorModel(HttpStatus.BAD_REQUEST, exception.getMessage());
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(mapper.valueToTree(error));
        }
    }

    @PostMapping("/repos")
    public ResponseEntity<JsonNode> addRepos(@RequestBody JsonNode json) {

        try {
            JsonNode res = gitlabRedisService.addRepos(json);
            logger.info("New gitlab repo added");

            return ResponseEntity.ok(res);
        }catch (IllegalArgumentException exception) {
            logger.error("Error with sent data: " + json.toString());
            ApiErrorModel error = new ApiErrorModel(HttpStatus.BAD_REQUEST, exception.getMessage());

            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(mapper.valueToTree(error));
        }catch (RuntimeException exception) {
            ApiErrorModel error = new ApiErrorModel(HttpStatus.INTERNAL_SERVER_ERROR, exception.getMessage());

            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(mapper.valueToTree(error));
        }
    }

}
