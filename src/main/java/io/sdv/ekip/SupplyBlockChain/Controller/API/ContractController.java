package io.sdv.ekip.SupplyBlockChain.Controller.API;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.sdv.ekip.SupplyBlockChain.Model.ContractInfoModel;
import io.sdv.ekip.SupplyBlockChain.Model.Error.ApiErrorModel;
import io.sdv.ekip.SupplyBlockChain.Service.ContractsManagerService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.web3j.crypto.Credentials;

import java.util.ArrayList;

@RestController
@RequestMapping("api/contract")
public class ContractController {

    Credentials credentials = Credentials.create("0x59edc805b16cc8c2d111a63c90337ab09868c010e0a96832ef422862c3a96553");

    private final Logger logger = LoggerFactory.getLogger(ContractController.class);

    private final ContractsManagerService contractsManager = new ContractsManagerService();
    private final ObjectMapper mapper = new ObjectMapper();


    @GetMapping("/latest")
    public ResponseEntity<JsonNode> getLastContract() {
        ContractInfoModel contract = null;

        try {
            contract = contractsManager.getLatestContract(this.credentials);
        } catch (Exception exception) {
            ApiErrorModel error = new ApiErrorModel(HttpStatus.BAD_REQUEST, exception.getMessage());
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(mapper.valueToTree(error));
        }

        if(contract == null) { //In case there is no contract in the blockchain
            ApiErrorModel error = new ApiErrorModel(HttpStatus.BAD_REQUEST, "Error while getting last contract, there is no contract or a problem when fetching it.");
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(mapper.valueToTree(error));
        }

        return ResponseEntity.ok(mapper.valueToTree(contract));
    }

    @GetMapping("/all")
    public ResponseEntity<JsonNode> getAllContract() {
        ArrayList<ContractInfoModel> contractInfoList = null;

        try {
            contractInfoList = contractsManager.getAllContract(this.credentials);
        } catch (Exception exception) {
            ApiErrorModel error = new ApiErrorModel(HttpStatus.BAD_REQUEST, exception.getMessage());
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(mapper.valueToTree(error));
        }

        return ResponseEntity.ok(mapper.valueToTree(contractInfoList));
    }

    @GetMapping("/table")
    public ResponseEntity<JsonNode> getTableInfo() {
        ArrayList<ContractInfoModel> contractInfoList = null;

        try {
            contractInfoList = contractsManager.getAllContract(this.credentials);
        } catch (Exception exception) {
            ApiErrorModel error = new ApiErrorModel(HttpStatus.BAD_REQUEST, exception.getMessage());
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(mapper.valueToTree(error));
        }

        return ResponseEntity.ok(mapper.valueToTree(contractInfoList));
    }

    @GetMapping("/{id}")
    public ResponseEntity<JsonNode> getContract(@PathVariable String id) {
        ContractInfoModel contract = null;

        try {
            contract = contractsManager.getContractByAddress(id, this.credentials);
        } catch (Exception exception) {
            ApiErrorModel error = new ApiErrorModel(HttpStatus.BAD_REQUEST, exception.getMessage());
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(mapper.valueToTree(error));
        }

        if(contract == null) { //In case there is no contract in the blockchain
            ApiErrorModel error = new ApiErrorModel(HttpStatus.BAD_REQUEST, "Error while getting contract with id: " + id + ", there is no contract or a problem when fetching it.");
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(mapper.valueToTree(error));
        }
        return ResponseEntity.ok(mapper.valueToTree(contract));
    }

    @GetMapping("/number")
    public ResponseEntity<JsonNode> getNumberOfContract() {
        try {
            int number = contractsManager.getNumberOfDeployedContracts();
            return ResponseEntity.ok( mapper.readTree("{\"number\":" + number + "}"));
        } catch (Exception exception) {
            ApiErrorModel error = new ApiErrorModel(HttpStatus.INTERNAL_SERVER_ERROR, exception.getMessage());
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(mapper.valueToTree(error));
        }
    }
}
