package io.sdv.ekip.SupplyBlockChain;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import io.sdv.ekip.SupplyBlockChain.Service.GitlabAPIService;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

import java.io.IOException;
import java.util.ArrayList;

@EnableScheduling
@SpringBootApplication
public class SupplyBlockChainApplication {

	public static void main(String[] args) throws IOException {
		SpringApplication.run(SupplyBlockChainApplication.class, args);
		GitlabAPIService gitlabAPIService = new GitlabAPIService();

		ArrayList<JsonNode> lesPipelines = gitlabAPIService.getSaFPipelines();
		System.out.println(lesPipelines.size());
	}
}
