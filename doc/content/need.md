# Content

* [Home](%base_url%/)
* [Dev blog](%base_url%/blog)

# Project Needs
## Establishing base functionality

At the start of the project and according to our understanding of the subject we made a brainstorm to extact the core feature needed for the POC.
We decided to focus on the blockchain part in the first place. So we extracted those features:

* Insert in the blockchain all and new commits.
* Insert in the blockchain all and new success and failed pipelines.
* Basic data visualization.

After establishing this basic list we searched for the best technologies that meets our needs. We choose to use [Ganache](https://www.trufflesuite.com/ganache) to handle the blokchain since everybody know it in the group. It allow us to quickly deploy a personal Ethereum blockchain. We use [Truffle](https://www.trufflesuite.com/truffle) as the same reason to handle the build of the smart contract.
  
Everyone in the group know java, so we decided to take [Spring Boot](https://spring.io/guides/gs/spring-boot/) as our backend. And we wanted to learn a new technology so for our frontend we decided to use [React](https://reactjs.org/) as it's a modern frontend framework and it respond to one of our need that our frontend should be updated asynchronously.
  
During the project planification and analysis, we found that the subject required to handle the blockchain part in the ci-cd process. But for reliability we decided to establish a new architecture based on multiple component to reduce project complexity and make it more efficient.

Link to the architecture explanation: [Architecture](%base_url%/architecture)