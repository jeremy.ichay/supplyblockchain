# Content

* [Home](%base_url%/)
* [Dev blog](%base_url%/blog)

# User Manual

Once the project is installed you can access it through IP address of the VM on port **8080**.
On the menu you can see the current repository that is setup and the number of contract (commit / pipeline) deployed.
An icon idicate if the application is running well.
  
If you launch the app by yourself should setup repository information (id and API key).

![Repo](%assets_url%/menu_repo.gif)

Sent informations are validated with a request on gitlab repository, to verify if repository id and API key is correct.

![Repo](%assets_url%/repo.gif)

To see all the deployed contract you should go here (**/blocks**):

![Repo](%assets_url%/menu_blocks.gif)

In this table there is all deployed contract and all information about it.
You can sort all the contracts by the time they where inserted in the blockchain, type and who made the change.

![Sort](%assets_url%/sort.gif)


Example of a contract that represent a commit:

![Commits](%assets_url%/commits.gif)


Example of a contract that represent a pipeline:

![Pipeline](%assets_url%/pipelines.gif)