# Content

* [Home](%base_url%/)
* [Dev blog](%base_url%/blog)


# Architecture
## Architecturing the project
The initial concept for this POC was to use ci-cd component of gitlab or other versioning software, to handle the data collect and data push on the blockchain. But after analyzing the need and the technical solution we decided to create a new architecture as follow:
  
![Architecture](%assets_url%/archi.png)
  
As you can see we try to make every component very independent that allow us to easily maintain our application.
In the middle we have our main application, with a scheduled job it's getting commits and pipelines result and storing it in ganache on the left.
 
We also have redis, it is used to store the repo informations like url of the repo and the secret key to call Gitlab API.
 
On the bottom we can find the workflow to construct our smart contract. Our contract is written in solidity and compile to an ABI in json with Truffle. Then we use [Web3j](http://web3j.io/) to convert this json into a java object that will be used to send contract to the blockchain.
Web3j is also used in our backend to communicate with ganache and exchange informations.
 
And the last component is the react frontend that interact with our API (backend).