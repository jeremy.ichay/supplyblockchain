# Project structure

You can find all the source code of our application in this repository:

https://gitlab.com/jeremy.ichay/supplyblockchain


![Architecture](%assets_url%/source.jpg)

You can find the source of the truffle contract in the folder **blockchain-code**.

The backend source code is in the **src** folder.

And the frontend application can be found in **ui/src** folder.

---

# Informations

User and password for the VM is `sdv` for both.

Main application is running in the vm on port **8080** by default. The blog documentation is running on port **80**.

VM IP address could be found by doing a `ifconfig`. If there is any problem, try changing the network interface / configuration Virtual Machine Software (Should be setup on bridge).

---

# Installation

## On given VM

In the given Virtual Machine you can found a script that will pull the last build version of the application and run it.   
Just run the following commands: `cd /home/sdv` and `./install.sh`.

## On personal env

### Java binary

You can get the lastest build of our application by running this command:  
``` wget --header "PRIVATE-TOKEN: apc6FZ4QqB1xqcP4PzUh" ```  
``` "https://gitlab.com/api/v4/projects/25469616/packages/generic/download/0.0.1/SupplyBlockChain-0.0.1-SNAPSHOT.jar" ```  
  
You should use Java JRE version 11 at least. https://adoptopenjdk.net/releases.html.

Then app can be launched by running `java -jar SupplyBlockChain-0.0.1-SNAPSHOT.jar`.

### Dev mode

To run the application in dev mode with the source code you need this requierements:
* **Ganache** (should be run with parameter `--mnemonic blockchain` to allways generate the same wallet as it's harcoded in the app).
* **Redis** to store repo information.
* **NodeJs** (for truffle and react).
* **Truffle** (should be install with npm).
* **Web3j** (Install info can be found [Here](https://github.com/web3j/web3j) in the README.md).
* **AdoptOpenJdk (HotSpot) 11** https://adoptopenjdk.net/
  
You should add a repo information in redis via our API.  
You can go to the interface and setup one from there. Url should be **VM_IP_ADDRESS:8080/repos**

