# Content

* [Home](%base_url%/)
* [Dev blog](%base_url%/blog)

# Project management

In this section there is a presentation of which tools we use to improve our workflow. We try to apply some good practices that we saw in our scrum course and internship.
We try to do this project with continuous improvment by dividing task into little easier tasks that everyone can do. In this way we can esaily see the progression of the project.

## Kanban

To track all of the different tasks that we have to do, we decided to use Trello. It allow us to know when someone finish a task and when to test it.  
This tools is very useful because we can easily see if there is a bottleneck on a certain task or part of the project.

![Kanban](%assets_url%/kanban1.jpg)
![Kanban](%assets_url%/kanban2.jpg)
![Kanban](%assets_url%/kanban3.jpg)

## Figma
  
We also decided to use figma to make basic wireframe so we can see what will look like the frontend and it allow us to understand which informations wil be needed. It also allow us to decompose the frontend in multiple sub component, that make React developpement much more easier.

![Figma](%assets_url%/menu.png)
![Figma](%assets_url%/repos.png)
![Figma](%assets_url%/table.png)