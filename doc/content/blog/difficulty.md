# Content

* [Home](%base_url%/)
* [Dev blog](%base_url%/blog)

# Encountered difficulties
  
During this project we encountered multipule difficulties on multiple subject. This section present one of them and how we solve it.

## Technical difficulties

### Performance issue

In the first version of the application we encountered a performance issue when getting all depoyed contract.
For 30 contract in the blockchain our API was taking 1.5s to fetch all the contract with their data.
So it was not very efficient. After a code analysis we found that the contract fetch could be more efficient.
  
##### Old algorithm

N -> number of block

- Get last block number (1 request)
- Get every blocks (N requests)
- For all blocks check if it's contract or a transaction (N requests)
- If it's a contract load it from the address in the block ((N / 2) requests)
  
With this method there is a lot HTTP request. For example for 30 blocks it gives -> 2 * 30 + 30 / 2 + 1 = 76 requests.

##### New algorithm

After searching on documentation and blockchain implementation we found that a contract address is deterministic. So with the right algorithm we can calculate and load the contract.

**The address for an Ethereum contract is deterministically computed from the address of its creator (sender) and how many transactions the creator has sent (nonce). The sender and nonce are RLP encoded and then hashed with Keccak-256.** from [Ethereum stack exchange](https://ethereum.stackexchange.com/questions/760/how-is-the-address-of-an-ethereum-contract-computed)

So the new algorithm is:
- Get number of deployed contracts (1 request)
- Calculate contract address
- Load Contracts (N / 2  requests)

![FetchCode](%assets_url%/fetch_code.png)

So for the same example with 30 blocks (15 contract), it gives -> 30 / 2 + 1 = 16 request.

#### Conclusion

With a good analysis we improve application performance by nearly 5 from the first version.

| Number of block | Old time | New time |
| --------------- |:--------:| --------:|
| 30              | 1.5s     | 0.360 ms |