# Content

* [Home](%base_url%/)
* [Architecture](%base_url%/architecture)

# Intro

When we started the project, we take a lot of time to analyze it, to be more efficient.
It was clear that the project was related to actuality with the recent Solarwinds attack. The main goal of that project was to create a POC of a software that can autheticate the supplychain of it. Because git is not the perfect software to prove the authenticity of modification in a project, an external tool could be created to handle that part in a blockchain. Bellow you can find the step that allow us to produce the POC named **SupplyBlockchain**.

1. [Definition of need / Technology](%base_url%/need)
2. [Architecturing the project](%base_url%/architecture)
3. [Project management](%base_url%/blog/management)
4. [Encountered difficulties](%base_url%/blog/difficulty)