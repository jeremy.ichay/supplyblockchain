import React, { useState, useEffect } from 'react';
import { DataGrid, GridOverlay } from '@material-ui/data-grid';
import Contract from '../Contract/Contract';
import Link from '@material-ui/core/Link';
import LinearProgress from '@material-ui/core/LinearProgress';
import Snackbar from '@material-ui/core/Snackbar';
import MuiAlert from '@material-ui/lab/Alert';

function Blockchain() {
  const [open, setOpen] = useState(false);
  const [rows, setRows] = useState([]);
  const [columns, setColumn] = useState([])
  const [contract, setContract] = useState(null);
  const [loading, setLoading] = useState(true);
  const [status, setStatus] = useState("");
  const [message, setMessage] = useState("");
  const [openAlert, setOpenAlert] = useState(false);

  const handleClickOpenAlert = () => {
    setOpenAlert(true);
  };
  
  const handleCloseAlert = (reason) => {
    if (reason === 'clickaway') {
      setOpenAlert(false)
      return;
    }

    setOpenAlert(false);
  };

  const handleClickOpen = () => {
    setOpen(true);
  };
  
  const handleClose = () => {
    setOpen(false);
  };

  useEffect(() => {
    setColumn([
      { field: 'type', headerName: 'Type', flex: 0.1 },
      { field: 'contratId', headerName: 'Id contract', flex: 0.4, renderCell: (params) => (
          <Link href="#" onClick={(e) => {
            e.preventDefault();
            handleClickOpen();
            fetch("/api/contract/" + params.value)
              .then(function(response) {
                if (!response.ok) {
                  throw Error(response.statusText);
                }
                return response.json();
              })
              .then(function(data) {
                setContract(data);
              })
              .catch(function(error) {
                setContract("error");
                console.log(error); 
              });

              
          }}>{params.value}</Link>) },
      { field: 'author', headerName: 'Author', flex: 0.2 },
      { field: 'insertedDate', headerName: 'Inserted Date', flex: 0.1 }
    ]);

    var tab = [];
    fetch("/api/contract/table")
      .then(function(response) {
        if (!response.ok) {
          throw response;
        }
        return response.json();
      })
      .then(function(data) {
        data.forEach(function callback(value, index) {
          var line = {id: index, type: value.contractType, contratId: value.contractAddresse, author: value.data.author, insertedDate: value.insertedDate};
          tab.push(line);
        });
        setRows(tab);
      })
      .catch(function(error) {
        if(error.toString() === "TypeError: Failed to fetch"){
          setMessage("Failed to fetch");
        } else {
          error.json().then(response => setMessage(response.error + response.message))
        }
        setStatus("error");
        handleClickOpenAlert();
      })
      .finally(() => setLoading(false));    
  }, [])

  return (
    <div style={{ height:"80vh"}}>
      <Contract contract={contract} open={open} onClose={handleClose}/>
      <DataGrid components={{LoadingOverlay: CustomLoadingOverlay,
        }}
        loading={loading}
         disableExtendRowFullWidth rows={rows} columns={columns}/>
      <Snackbar open={openAlert} autoHideDuration={6000} onClose={handleCloseAlert}>
        <Alert onClose={handleCloseAlert} severity={status}>{message}</Alert>
      </Snackbar>
    </div>
  )
}

export default Blockchain;

function CustomLoadingOverlay() {
  return (
    <GridOverlay>
      <div style={{ position: 'absolute', top: 0, width: '100%' }}>
        <LinearProgress />
      </div>
    </GridOverlay>
  );
}

function Alert(props) {
  return <MuiAlert variant="filled" {...props} />;
}