import styled from 'styled-components';

export const StyledRepos = styled.main`
  text-align: center;
  height: 80vh;
  display: flex;
  justify-content: center;
  align-items: center;
`;