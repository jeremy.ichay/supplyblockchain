import React, { useState, useEffect } from 'react';
import { StyledRepos } from './Repos.styled';
import TextField from '@material-ui/core/TextField';
import Box from '@material-ui/core/Box';
import Button from '@material-ui/core/Button';
import { Typography } from '@material-ui/core';
import Divider from '@material-ui/core/Divider';
import Snackbar from '@material-ui/core/Snackbar';
import MuiAlert from '@material-ui/lab/Alert';

function Repos() {
  const [repo, setRepo] = useState("");
  const [apiKey, setApiKey] = useState("");
  const [currentRepo, setCurrentRepo] = useState("Loading...");
  const [lastContract, setLastContract] = useState("Loading...");
  const [open, setOpen] = useState(false);
  const [status, setStatus] = useState("");
  const [message, setMessage] = useState("");

  const handleClick = () => {
    setOpen(true);
  };

  const handleClose = (reason) => {
    if (reason === 'clickaway') {
      setOpen(false)
      return;
    }

    setOpen(false);
  };

  useEffect(() => {
    fetch("/api/gitlab/repos/current")
      .then(function(response) {
        if (!response.ok) {
          throw Error(response.statusText);
        }
        return response.json();
      })
      .then(data => {
        if(data.id === null) {
          setCurrentRepo("No repo id has been defined");
        } else {
          setCurrentRepo(data.id)
        }
      })
      .catch(function(response) {
        setCurrentRepo("Can't retrieve repo id")
        console.log(response);
      });
        
    fetch("/api/contract/latest")
      .then(function(response) {
        if (!response.ok) {
          throw Error(response.status);
        }
        return response.json();
      })
      .then(data => setLastContract(data.contractAddresse))
      .catch(function(response) {
        setLastContract("Can't retrieve last contract id")
        console.log(response);
      });
  }, [])

  const handleSubmit = (evt) => {
    evt.preventDefault();
    const requestOptions = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify({ id: repo, apiKey: apiKey })
    };
    fetch('/api/gitlab/repos/', requestOptions)
      .then(function(response) {
        if (!response.ok) {
          throw response;
        }
        return response.json();
      })
      .then(data => {
        setCurrentRepo(data.id);
        setStatus("success");
        setMessage("Repository successfully added");
      })
      .catch(function(error) {
        if(error.toString() === "TypeError: Failed to fetch"){
          setMessage("Failed to fetch");
        } else {
          error.json().then(response => setMessage(response.message))
        }
        setStatus("error");
      })
      .finally(() => handleClick());
  }
  return (
    <StyledRepos>
      <div>
        <Typography variant="h6" component="h6" style={{textAlign:"left"}}>Current Repo id</Typography>
        <Typography component="div" gutterBottom> 
          <Box border={1} borderColor="primary.main" color="text.primary" borderRadius={5} p={1}>{currentRepo}</Box>
        </Typography>

        <Typography variant="h6" component="h6" style={{textAlign:"left"}}>Last contract id</Typography>
        <Typography component="div" gutterBottom>
          <Box mb={5} border={1} borderColor="primary.main" color="text.primary" borderRadius={5} p={1}>{lastContract}</Box>
        </Typography>
        <Divider />
        <Box mt={5}>
        <Typography variant="h6" component="h6" style={{textAlign:"left"}}>Add repo</Typography>
          <form onSubmit={handleSubmit}>
              <TextField margin='dense' fullWidth id="urlInput" label="Repo id" variant="outlined" required onChange={e => setRepo(e.target.value)} />
              <TextField margin='dense' fullWidth id="apiKeyInput" label="Api key" variant="outlined" required onChange={e => setApiKey(e.target.value)} />
              <Button variant="contained" color="primary" type="submit" onChange={e => []}>Submit</Button>
          </form>
        </Box>
      </div>
      <Snackbar open={open} autoHideDuration={6000} onClose={handleClose}>
        <Alert onClose={handleClose} severity={status}>{message}</Alert>
      </Snackbar>
    </StyledRepos>
  )
}

export default Repos;

function Alert(props) {
  return <MuiAlert variant="filled" {...props} />;
}