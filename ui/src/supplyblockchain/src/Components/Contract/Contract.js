import React from 'react';
import DialogTitle from "@material-ui/core/DialogTitle";
import Dialog from "@material-ui/core/Dialog";
import DialogContent from '@material-ui/core/DialogContent';
import Typography from '@material-ui/core/Typography';
import CheckCircleOutlineIcon from '@material-ui/icons/CheckCircleOutline';
import CancelOutlinedIcon from '@material-ui/icons/CancelOutlined';
import RemoveCircleOutlineOutlinedIcon from '@material-ui/icons/RemoveCircleOutlineOutlined';
import List from '@material-ui/core/List';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';

function Contract(props) {
  const { onClose, open } = props;

  const handleClose = () => {
    onClose();
  };

  if(props.contract === null){
    return (
      <Dialog onClose={handleClose} open={open}>
        <DialogContent dividers>
          <Typography gutterBottom>
            Contract is loading
          </Typography>
        </DialogContent>
      </Dialog>
    )
  }
  
  if(props.contract.contractType === "COMMIT") {
    return (
      <Dialog onClose={handleClose} open={open}>
        <DialogTitle>{props.contract.contractAddresse}</DialogTitle>
        <DialogContent dividers>
          <Typography gutterBottom>
          <span style={{fontWeight:"bold"}}>Contract Type:</span> {props.contract.contractType}
          </Typography>
          <Typography gutterBottom>
            <span style={{fontWeight:"bold"}}>Commit Id:</span> {props.contract.data.id}
          </Typography>
          <Typography gutterBottom>
            <span style={{fontWeight:"bold"}}>Author:</span> {props.contract.data.author}
          </Typography>
          <Typography gutterBottom>
            <span style={{fontWeight:"bold"}}>Commit Date:</span> {props.contract.data.commitDate}
          </Typography>
          <Typography gutterBottom>
            <span style={{fontWeight:"bold"}}>Commit Message:</span> {props.contract.data.message}
          </Typography>
        </DialogContent>
      </Dialog>
    )
  }

  if(props.contract.contractType === "PIPELINE") {
    const jobs = props.contract.data.jobs;
    const listJobs = jobs.map((jobs) => 
      <ListItem key={`item-${jobs.jobName}`}>
        <ListItemIcon>
          {jobs.jobStatus === "failed" &&
            <CancelOutlinedIcon fontSize="large" style={{color: "red",}}/>
          }
          {jobs.jobStatus === "success" &&
            <CheckCircleOutlineIcon fontSize="large" style={{color: "green", }}/>
          }
          {jobs.jobStatus === "canceled" &&
            <RemoveCircleOutlineOutlinedIcon fontSize="large" style={{color: "gray", }}/>
          }
        </ListItemIcon>
        <ListItemText
          primary={jobs.jobName}  
          secondary={jobs.jobStatus}
        />
      </ListItem>
    );
    return (
      <Dialog onClose={handleClose} open={open}>
      <DialogTitle>{props.contract.contractAddresse}</DialogTitle>
      <DialogContent dividers>
        <Typography gutterBottom>
          <span style={{fontWeight:"bold"}}>Contract Type:</span> {props.contract.contractType}
        </Typography>
        <Typography gutterBottom>
          <span style={{fontWeight:"bold"}}>Id:</span> {props.contract.data.id}
        </Typography>
        <Typography gutterBottom>
          <span style={{fontWeight:"bold"}}>Author:</span> {props.contract.data.author}
        </Typography>
        <Typography gutterBottom>
          <span style={{fontWeight:"bold"}}>End Date:</span> {props.contract.data.endDate}
        </Typography>
        <Typography gutterBottom>
          <span style={{fontWeight:"bold"}}>Status:</span> {props.contract.data.status}
        </Typography>
        <Typography gutterBottom>
          <span style={{fontWeight:"bold"}}>Jobs:</span> 
        </Typography>
        <List dense disablePadding>
          {listJobs}
        </List>

      </DialogContent>
    </Dialog>
    )
  }
  return (
    <Dialog onClose={handleClose} open={open}>
      <DialogContent dividers>
        <Typography gutterBottom>
          Error
        </Typography>
      </DialogContent>
    </Dialog>
  )
}

export default Contract;