import React, { useState, useEffect } from 'react';
import { Typography } from '@material-ui/core';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Grid from '@material-ui/core/Grid';
import WidgetsIcon from '@material-ui/icons/Widgets';
import Link from '@material-ui/core/Link';
import SentimentVerySatisfiedIcon from '@material-ui/icons/SentimentVerySatisfied';
import MoodBadIcon from '@material-ui/icons/MoodBad';

function Home() {
    const [name, setName] = useState("Loading");
    const [url, setUrl] = useState();
    const [number, setNumber] = useState(0);
    const [status, setStatus] = useState(false);
     
    useEffect(() => {
        fetch("/api/gitlab/repos/info")
        .then(function(response) {
            if (!response.ok) {
                throw Error(response.statusText);
            }
            return response.json();
        })
        .then(data => {
            setName(data.name);
            setUrl(data.web_url);
            setStatus(true);
            console.log(data);
        })
        .catch(function(response) {
            setName("None");
            console.log(response);
        });

        fetch("/api/contract/number")
        .then(function(response) {
            if (!response.ok) {
                throw Error(response.statusText);
            }
            return response.json();
        })
        .then(data => {
            setNumber(data.number);
        })
        .catch(function(response) {
            console.log(response);
        });
    }, [])
  
    return (
        <Card >
            <CardContent>
                <Grid container spacing={3}>
                    <Grid item xs={4}>
                        <Typography  color="textSecondary" gutterBottom>
                            Current repo
                        </Typography>
                        <Typography variant="h5" component="h2">
                            {name}
                        </Typography>
                        <Typography gutterBottom>
                            <Link color="textSecondary" href={url} target="_blank">{url}</Link>
                        </Typography>
                    </Grid>
                    <Grid item xs={4}>
                        <Typography variant="h2" component="div" align="center" >
                            {number} <WidgetsIcon style={{ fontSize: 80, color:"rgba(0, 0, 0, 0.54)" }}/>
                        </Typography>
                    </Grid>
                    {status === true &&
                        <Grid item xs={4}>
                            <Typography align="center">
                                <SentimentVerySatisfiedIcon style={{ fontSize: 80, color:"rgb(76, 175, 80)" }}/>
                            </Typography>
                        </Grid>
                    }
                    {status === false &&
                        <Grid item xs={4}>
                            <Typography align="center">
                                <MoodBadIcon style={{ fontSize: 80, color:"rgb(244, 67, 54)" }}/>
                            </Typography>
                        </Grid>
                    }
                </Grid>                
            </CardContent>
        </Card>
  )
}

export default Home;