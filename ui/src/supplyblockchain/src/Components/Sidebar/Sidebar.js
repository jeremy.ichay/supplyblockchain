import React, { useState } from 'react';
import Menu from '../Menu/Menu';

function Sidebar() {
  const [open, setOpen] = useState(false);
  const menuId = "main-menu";

  return (
    <div>
      <Menu open={open} setOpen={setOpen} id={menuId} />
    </div>
   );
}

export default Sidebar;